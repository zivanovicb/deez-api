const amqp = require("amqplib");
const RMQ_URL = "amqp://localhost";
const ADD_USER_QUEUE = "add_user_queue";

async function main() {
  try {
    const conn = await amqp.connect(RMQ_URL);
    const ch = await conn.createChannel();
    await ch.assertQueue(ADD_USER_QUEUE);
    ch.consume(ADD_USER_QUEUE, msg => {
      console.log(msg.content.toString());
    });
  } catch (err) {
    console.error(err);
  }
}

main();
