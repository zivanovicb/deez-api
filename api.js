const axios = require("axios");

const API_URL = "http://localhost:3000/api";

const api = {
  getRoom: function(roomId) {
    return axios.get(`${API_URL}/rooms/${roomId}`);
  }
};

module.exports = api;
