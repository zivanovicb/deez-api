const {
	getAsync,
	hgetallAsync,
	zRangeAsync,
	hmsetAsync,
	sAddAsync,
	sRemAsync,
	sMembersAsync,
	delAsync
} = require("./redis");
const async = require("async");
const utils = require("./utils");
const JOIN_ROOM = "JOIN_ROOM";

const QUEUE_LIST = "QUEUE_LIST";
const ONLINE_SOCKETS = "ONLINE_SOCKETS";

// Track data are stored like this
// TRACK:${TRACK_ID}
// and these are redis hashes

function get_room_state() {
	return new Promise(async (resolve, reject) => {
		try {
			const response = {
				online_sockets: [],
				current_track_data: null,
				current_track_timestamp: null,
				queue: []
			};

			console.log("[get_room_state] looking for online sockets in: ", ONLINE_SOCKETS);
			const online_sockets = await sMembersAsync(ONLINE_SOCKETS);
			console.log("[get_room_state] online_sockets: ", online_sockets);
			// ONLINE_SOCKETS
			response.online_sockets = online_sockets;

			const current_track_id = await getAsync("current_track");
			if (current_track_id) {
				console.log("[get_room_state] current_track id: ", current_track_id);
				const current_track_data = hgetallAsync(`TRACK:${current_track_id}`);
				console.log("[get_room_state] current_track_data: ", current_track_data);
				// CURRENT_TRACK
				response.current_track_data = current_track_data;
				const current_track_timestamp = await getAsync(`TRACK:${current_track_id}`);
				// CURRENT_TRACK_TIMESTAMP
				response.current_track_timestamp = current_track_timestamp;
			} else {
				console.log("[get_room_state] current_track_id is null or undefined ", current_track_id);
			}

			// [id, score, id, score]
			const queue_track_ids_with_scores = await zRangeAsync(QUEUE_LIST, 0, -1, "withscores");

			const queue_track_ids = utils.transform_track_ids(queue_track_ids_with_scores);
			// [{id: track_id,score: xy }]
			console.log("[add_track_to_queue] TRANSFORMED queue_track_ids: ", queue_track_ids);

			console.log("[get_room_state] queue_ids ", queue_track_ids);
			const queue = await get_queue_with_track_data(queue_track_ids);
			console.log("[get_room_state] queue: ", queue);
			// QUEUE
			response.queue = queue;
			resolve(response);
		} catch (err) {
			reject(err);
			console.error("[get_room_state] err: ", err);
		}
	});
}

function get_queue_with_track_data(queue_ids_with_scores) {
	console.log("[get_queue_with_track_data] called. queue_ids_with_scores: ", queue_ids_with_scores);
	return new Promise(async (resolve, reject) => {
		try {
			const fns = [];
			const providerFn = cb => {
				cb(null, { ind: 0, queue_ids_with_scores, queue_tracks: [] });
			};

			fns.push(providerFn);

			const get_track_data = (data, cb) => {
				console.log("dejta ", data);
				const { ind } = data;
				if (queue_ids_with_scores[ind]) {
					let track_id_with_score = queue_ids_with_scores[ind];
					console.log("[get_track_data] HASHKEY ", `TRACK:${track_id_with_score.id}`);

					hgetallAsync(`TRACK:${track_id_with_score.id}`)
						.then(track_data => {
							console.log("[get_queue_with_track_data] track_data: ", track_data);
							cb(null, { ind: ind + 1, queue_tracks: [...data.queue_tracks, track_data] });
						})
						.catch(err => {
							console.log("sta je ", err);
							cb(err);
							reject(err);
						});
				}
			};

			for (let i = 0; i < queue_ids_with_scores.length; i++) {
				fns.push(get_track_data);
			}

			async.waterfall(fns, (err, result) => {
				console.log("[async.waterfall] err: ", err);
				console.log("[async.waterfall] result: ", result);

				if (!err) {
					resolve(result.queue_tracks);
				} else resolve(result);
			});
		} catch (err) {
			reject(err);
			console.error(err);
		}
	});
}

function socket_join(socket) {
	return new Promise(async (resolve, reject) => {
		try {
			console.log("[socket_join] socket.id: ", socket.id);
			await sAddAsync(ONLINE_SOCKETS, socket.id);
			const online = await sMembersAsync(ONLINE_SOCKETS);
			console.log("[socket_join] Online users", online);
			resolve(online);
		} catch (err) {
			reject(err);
			console.error(err);
		}
	});
}

function socket_left(socket) {
	return new Promise(async (resolve, reject) => {
		try {
			console.log("[socket_left] socket.id: ", socket.id);
			const removed_user = sRemAsync(ONLINE_SOCKETS, socket.id);
			console.log("[socket_left] removed_user: ", removed_user);
			const online = await sMembersAsync(ONLINE_SOCKETS);
			resolve(online);
		} catch (err) {
			reject(err);
			console.error(err);
		}
	});
}

module.exports = {
	socket_join,
	socket_left,
	get_room_state,
	get_queue_with_track_data
};
