const { getAsync, incrByAsync } = require("./redis");
const { CURRENT_TRACK_TIMESTAMP } = require("./types");

function transform_track_ids(queue_track_ids_with_scores) {
	return queue_track_ids_with_scores
		.map((item, i) => {
			if (i % 2 === 0) {
				return {
					id: item,
					score: queue_track_ids_with_scores[i + 1]
				};
			}
		})
		.filter(item => !!item);
}

async function interval_count(intervals, track_id, track_length) {
	const current_track_timestamp = await getAsync(CURRENT_TRACK_TIMESTAMP);
	if (current_track_timestamp < track_length) {
		const incremented_key = await incrByAsync(CURRENT_TRACK_TIMESTAMP, 1);
		console.log("[add_track_to_queue] incremented_key: ", incremented_key);
	} else {
		console.log(
			"[add_track_to_queue] current_track_stamp is equal or higher than track length ::: ",
			current_track_timestamp
		);
		console.log("[add_track_to_queue] clearing interval");
		clearInterval(intervals[track_id]);
	}
}
module.exports = {
	transform_track_ids,
	interval_count
};
