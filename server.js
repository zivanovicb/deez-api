const app = require("express")();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
const cors = require("cors");
const axios = require("axios");
const {
	sign_socket_in,
	sign_socket_out,
	play_track,
	add_track_to_queue,
	remove_track_from_queue
} = require("./event_handlers");
const { ROOM_JOIN, DISCONNECT, ADD_TO_QUEUE, PLAY_TRACK, REMOVE_TRACK_FROM_QUEUE } = require("./types");
const port = "3080";

const intervals = {};

app.use(cors());
app.get("/api/search", async (req, res) => {
	const { q } = req.query;
	try {
		const r = await axios.get(`https://api.deezer.com/search?q=${q}`);
		return res.status(200).json(r.data.data);
	} catch (err) {
		console.log("err: ", err);
	}
});

app.get("/api/charts", async (req, res) => {
	try {
		const r = await axios.get("https://api.deezer.com/chart");
		return res.status(200).json({ tracks: r.data.tracks.data });
	} catch (err) {
		return res.status(500).json({ err });
	}
});

io.on("connection", socket => {
	console.log("[Connesct] socket.id: ", socket.id);

	socket.on(ROOM_JOIN, () => {
		sign_socket_in(socket);
	});

	socket.on(DISCONNECT, () => {
		sign_socket_out(socket);
	});

	socket.on(ADD_TO_QUEUE, ({ id, artist, length, name, thumbnail }) => {
		add_track_to_queue(socket, intervals, id, name, length, thumbnail, artist);
	});
	socket.on(PLAY_TRACK, ({ id, artist, length, name, thumbnail }) => {
		console.log("PLAY_TRACK EV ", id, artist, length, name, thumbnail);
		play_track(socket, intervals, id, artist, length, name, thumbnail);
	});

	socket.on(REMOVE_TRACK_FROM_QUEUE, id => {
		remove_track_from_queue(socket, id);
	});
});

server.listen(port, () => console.log(`Example app listening on port ${port}!`));
