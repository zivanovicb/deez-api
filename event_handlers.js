const courier = require("./courier");

const {
	sAddAsync,
	sMembersAsync,
	sRemAsync,
	zRankAsync,
	zRangeAsync,
	zAddAsync,
	zCountAsync,
	hmsetAsync,
	getAsync,
	incrByAsync,
	hgetallAsync,
	zRemAsync,
	hmgetAsync
} = require("./redis");

const {
	QUEUE_LIST,
	CURRENT_TRACK_TIMESTAMP,
	ONLINE_SOCKETS,
	SOCKETS_UPDATED,
	ROOM_JOIN_SUCCESSFULL,
	APP_ERROR,
	CURRENT_TRACK_DATA,
	PLAY_TRACK,
	QUEUE_LIST_UPDATED
} = require("./types");

const utils = require("./utils");

async function sign_socket_in(socket) {
	try {
		// Add socket to set
		console.log("[sign_socket_in] adding socket in: ", ONLINE_SOCKETS);
		const socket_added = await sAddAsync(ONLINE_SOCKETS, socket.id);
		console.log("[sign_socket_in] socket_added: ", socket_added);

		// Get online_ockets, queue, current_track_data, current_track_timestamp
		const room_state = await courier.get_room_state();
		console.log("[sign_socket_in] room_state: ", room_state);

		// Goes to the recently connected socket
		socket.emit(ROOM_JOIN_SUCCESSFULL, room_state);

		// Goes to all sockets except the recently connected one
		socket.broadcast.emit(SOCKETS_UPDATED, { online_sockets: room_state.online_sockets });
	} catch (err) {
		console.log("[sign_socket_in] app_error: ", err);
		socket.emit(APP_ERROR);
	}
}

async function sign_socket_out(socket) {
	try {
		console.log("[sign_socket_out] called");
		const socket_removed = await sRemAsync(ONLINE_SOCKETS, socket.id);
		console.log("[sign_socket_out] socket_removed: ", socket_removed);

		const online_sockets = await sMembersAsync(ONLINE_SOCKETS);
		console.log("[sign_socket_out] online_sockets: ", online_sockets);

		socket.broadcast.emit(SOCKETS_UPDATED, { online_sockets });
	} catch (err) {
		console.log("[sign_socket_out] app_error: ", err);
		socket.emit(APP_ERROR);
	}
}

// CURRENT_TRACK =
async function play_track(socket, intervals, id, artist, length, name, thumbnail) {
	try {
		console.log("[play_track] current intervals: ", intervals);
		console.log("[play_track] data: ", socket, id, artist, length, name, thumbnail);

		if (intervals.hasOwnProperty(id)) {
			console.log("[play_track] Intervals has it. Clearing... ");
			clearInterval(intervals[id]);
		}

		const current_track_data_inserted = await hmsetAsync(
			CURRENT_TRACK_DATA,
			"id",
			id,
			"name",
			name,
			"thumbnail",
			thumbnail,
			"artist",
			artist,
			"length",
			length
		);

		console.log("[play_track] current_track_data_inserted: ", current_track_data_inserted);

		// CURRENT_TRACK_DATA
		const current_track_data = await hgetallAsync(CURRENT_TRACK_DATA);
		console.log("[play_track] current_track_data: ", current_track_data);

		if (!intervals.hasOwnProperty(id)) {
			console.log("[play_track] intervals: ", intervals);
			console.log("[play_track] intervals does not have id: ", id);
			intervals[id] = setInterval(() => {
				// async function interval_count(intervals, track_id, track_length) {
				utils.interval_count(intervals, id, length);
			}, 1000);
		}

		// QUEUE
		const queue_track_ids_with_scores = await zRangeAsync(QUEUE_LIST, 0, -1, "withscores");

		// [id,index,id,index]
		const queue_track_ids = utils.transform_track_ids(queue_track_ids_with_scores);
		// [{id: track_id,score: xy }]
		console.log("[play_track] TRANSFORMED queue_track_ids: ", queue_track_ids);
		const queue = await courier.get_queue_with_track_data(queue_track_ids);
		console.log("[play_track] queue: ", queue);

		console.log("[play_track] Emitting: ", QUEUE_LIST_UPDATED);
		// Emitting to ourselves
		socket.emit(QUEUE_LIST_UPDATED, queue);
		// Emitting to others
		socket.broadcast.emit(QUEUE_LIST_UPDATED, queue);

		console.log("[play_track] emitting: ", PLAY_TRACK);
		// Emitting to ourselves
		socket.emit(PLAY_TRACK, { CURRENT_TRACK_DATA: current_track_data });
		// Emitting to others
		socket.broadcast.emit(PLAY_TRACK, { CURRENT_TRACK_DATA: current_track_data });
	} catch (err) {
		console.error("[play_track] err: ", err);
	}
}

// add_track_to_queue(socket, track_id, track_name, track_thumbnail, track_artist);
async function add_track_to_queue(
	socket,
	intervals,
	track_id,
	track_name,
	track_length,
	track_thumbnail,
	track_artist
) {
	try {
		// QUEUE LIST COUNT
		const queue_list_count = await zCountAsync(QUEUE_LIST, "-inf", "+inf");
		console.log("[add_track_to_queue] queue_list_count: ", queue_list_count);

		console.log("[add_track_to_queue] data: ", track_id, track_name, track_length, track_thumbnail, track_artist);

		// TRACK TO QUEUE LIST(SORTED SET)
		const track_added_to_queue = await zAddAsync(QUEUE_LIST, queue_list_count + 1, track_id);
		console.log("[add_track_to_queue] track_added_to_queue: ", track_added_to_queue);

		const TRACK_HASH_KEY = `TRACK:${track_id}`;
		// TRACK DATA
		const track_inserted = await hmsetAsync(
			TRACK_HASH_KEY,
			"id",
			track_id,
			"name",
			track_name,
			"thumbnail",
			track_thumbnail,
			"artist",
			track_artist,
			"length",
			track_length
		);
		console.log("[add_track_to_queue] track_inserted: ", track_inserted);

		if (queue_list_count === 0) {
			// CURRENT TRACK DATA
			const current_track_data_inserted = await hmsetAsync(
				CURRENT_TRACK_DATA,
				"id",
				track_id,
				"name",
				track_name,
				"thumbnail",
				track_thumbnail,
				"artist",
				track_artist,
				"length",
				track_length
			);
			console.log("[add_track_to_queue] current_track_data_inserted: ", current_track_data_inserted);

			const current_track_data = await hgetallAsync(CURRENT_TRACK_DATA);
			console.log("[add_track_to_queuegot current_track_data: ", current_track_data_inserted);

			console.log("[add_track_to_queue] emitting: ", PLAY_TRACK);
			socket.emit(PLAY_TRACK, { CURRENT_TRACK_DATA: current_track_data });

			intervals[track_id] = setInterval(async () => {
				// async function interval_count(intervals, track_id, track_length) {
				utils.interval_count(intervals, track_id, track_length);
			}, 1000);
		} else {
			console.log("[add_track_to_queue] there are " + queue_list_count + " tracks in queue");
		}

		// QUEUE
		const queue_track_ids_with_scores = await zRangeAsync(QUEUE_LIST, 0, -1, "withscores");

		// [id,index,id,index]
		const queue_track_ids = utils.transform_track_ids(queue_track_ids_with_scores);
		// [{id: track_id,score: xy }]
		console.log("[add_track_to_queue] TRANSFORMED queue_track_ids: ", queue_track_ids);
		const queue = await courier.get_queue_with_track_data(queue_track_ids);
		console.log("[add_track_to_queue] queue: ", queue);

		// Emitting to ourselves
		socket.emit(QUEUE_LIST_UPDATED, queue);
		// Emitting to others
		socket.broadcast.emit(QUEUE_LIST_UPDATED, queue);
	} catch (err) {
		console.log("[add_track_to_queue] app_error: ", err);
		socket.emit(APP_ERROR);
	}
}

async function remove_track_from_queue(socket, id) {
	try {
		console.log("[remove_track_from_queue] track id to be delted: ", id);
		const removed_track_from_queue = await zRemAsync(QUEUE_LIST, id);
		console.log("[remove_track_from_queue] removed_track_from_queue: ", removed_track_from_queue);

		// QUEUE
		const queue_track_ids_with_scores = await zRangeAsync(QUEUE_LIST, 0, -1, "withscores");

		// [id,index,id,index]
		const queue_track_ids = utils.transform_track_ids(queue_track_ids_with_scores);
		// [{id: track_id,score: xy }]
		console.log("[add_track_to_queue] TRANSFORMED queue_track_ids: ", queue_track_ids);
		const queue = await courier.get_queue_with_track_data(queue_track_ids);
		console.log("[add_track_to_queue] queue: ", queue);

		// Emitting to ourselves
		socket.emit(QUEUE_LIST_UPDATED, queue);
		// Emitting to others
		socket.broadcast.emit(QUEUE_LIST_UPDATED, queue);
	} catch (err) {
		console.error("[remove_track_from_queue] err: ", err);
	}
}
module.exports = {
	sign_socket_in,
	sign_socket_out,
	add_track_to_queue,
	remove_track_from_queue,
	play_track
};
